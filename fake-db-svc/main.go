package main

import (
	"fmt"
	"net/http"
)

func returnSomething(w http.ResponseWriter, req *http.Request) {
	fmt.Fprintf(w, "the fake db service is running\n")
}

func main() {
	fmt.Println("fake-db-svc is running...")
	http.HandleFunc("/", returnSomething)
	http.ListenAndServe(":8080", nil)
}
