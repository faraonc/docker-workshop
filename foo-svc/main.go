package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
)

func hello(w http.ResponseWriter, req *http.Request) {
	fmt.Fprintf(w, "hello\n")
}

func headers(w http.ResponseWriter, req *http.Request) {
	for name, headers := range req.Header {
		for _, h := range headers {
			fmt.Fprintf(w, "%v: %v\n", name, h)
		}
	}
}

func pingFakeDBSvc(w http.ResponseWriter, req *http.Request) {
	fakeSvc := os.Getenv("FAKE_SVC")
	fakeSvcHost := fmt.Sprintf("http://%s:8080/fase", fakeSvc)
	resp, err := http.Get(fakeSvcHost)
	if err != nil {
		fmt.Fprintf(w, "fake-db-svc is unreachable\n")
		return
	}
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Fprintf(w, "unable to read fake-db-svc response\n")
		return
	}
	fmt.Fprint(w, string(b))
}

func main() {
	fmt.Println("foo-svc is running...")
	http.HandleFunc("/hello", hello)
	http.HandleFunc("/headers", headers)
	http.HandleFunc("/fake", pingFakeDBSvc)
	http.ListenAndServe(":8090", nil)
}
