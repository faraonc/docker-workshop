# docker-workshop

A Docker workshop I hosted for https://uwblockchainhacks.devpost.com/

## To build and run the images
``` bash
# using docker-compose
$ docker-compose up
# don't forget to run $ docker-compose down

# to check if services are runnin
$ curl localhost:8090/hello
$ curl localhost:8090/fake
$ curl localhost:8080
```

